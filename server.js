const express = require('express');
const app = express();
const path = require('path');
const cookieParser = require('cookie-parser');
const session = require('express-session');

// Enable Request Cookies
app.use(cookieParser());

// Enable Server Sessions
app.use(session({
    secret: 'superdupersecretsecret',
    resave: false,
    saveUninitialized: false,
    cookie: {
        secure: false,
        httpOnly: true,
        maxAge: 360000
    }
}));

app.get('/', (req, res) => {
    if (req.session.counter == undefined) {
        res.redirect('/login');

    }else {
        res.redirect('/Dashboard');
    }
    
})

app.get('/create', (req, res) => {
    req.session.counter = 1;
    res.send(`<h2>Session was created...</h2>
    <p> Press the button to return to the start page. </p>
    <a href="/"><button>Click me!</button></a>`);
})

app.get('/login', (req, res) => {
    res.send(`<h2>Welcome to the login page...</h2>
    <p> Please press the button to be redirected to the creation page...</p>
    <a href="/create"><button>Click me!</button></a>`);
})

app.get('/dashboard', (req, res) => {
    res.send('<h2>Dashboard</h2>');
})

app.listen(3000, () => console.log("App started on port 3000..."));